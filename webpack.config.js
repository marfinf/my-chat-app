const path = require('path');

module.exports = {
    entry: './src/index.js', // or your entry file
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            // your loaders
        ]
    },
    resolve: {
        fallback: {
            util: require.resolve('util/')
        }
    },
    // other configuration settings...
};
