import React, { useState } from 'react';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
import FileUpload from './components/fileupload/FileUpload';
import { BASE_URL } from './constant';
import Button from './components/button/Button';
import { FaAutoprefixer } from 'react-icons/fa';


function App() {
  const [input, setInput] = useState('');
  const [loading, setLoading] = useState(false);
  const [loadingText, setLoadingText] = useState("Please wait...");
  const [downloadable, setDownloadable] = useState(false);
  const [requestForDownload, setRequestForDownload] = useState('');

  const getRandomString = (length) => {
    console.log(process.env.REACT_APP_STAGE)
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };
  const [rndStr, setRndStr] = useState(getRandomString(10));
  const whatsappUrl = `https://web.whatsapp.com/send?phone=+6281908878120&text=${encodeURIComponent("Halo pak Marfin, saya mengalami kendala ")}`;


  const handleDoc = async (e) => {
    setDownloadable(false);
    setLoading(true)
    e.preventDefault();

    setLoadingText("Generating your document...")

    try {
      const response = await axios.post(`${BASE_URL}/generate-doc`, {
        inputString: input,
        key: rndStr
      });
      if (response.data) {
        console.log("set download answer")
        console.log(response.data)
        setDownloadable(true);
        setLoading(false);
        setRequestForDownload(response.data)
      }
    } catch (error) {
      console.error(error);
      setLoadingText(error)
      setLoading(false)
    }
  }

  const handleDownload = async () => {
    setLoading(true)
    setLoadingText("Downloading your document...")

    try {
      const response = await axios.get(`${BASE_URL}/download-doc`, {
        params: {
          key: rndStr,
        },
        responseType: 'blob',
      });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'output.docx');
      document.body.appendChild(link);
      link.click();
      link.parentNode.removeChild(link);
      setLoadingText("Please wait...")
    } catch (error) {
      console.error("There was an error downloading the document!", error);
    } finally {
      setLoading(false)
    }
  };

  const handleUpload = async (file) => {
    const formData = new FormData();
    formData.append('file', file);
    setLoading(true)
    setLoadingText("Uploading your file... ")

    try {
      const response = await axios.post(`${BASE_URL}/upload?key=${rndStr}&input=${input}`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      if (response.data) {
        setLoadingText('');
        setDownloadable(true);
        setLoading(false);
        setRequestForDownload(response.data)
      }
    } catch (error) {
      setLoadingText('Failed to upload file');
    } finally {
      setLoading(false)
    }
  };

  return (
    <div style={styles.outerContainer}>
      <div style={styles.container}>
        <div style={styles.container12}>
          <img src="/logo192.png" alt="Logo" width={50} height={50} />
          <h1 style={styles.header}>TangDoc A.I</h1>
        </div>
        <form style={styles.form}>
          <textarea
            value={input}
            onChange={(e) => setInput(e.target.value)}
            rows="10"
            cols="50"
            placeholder="Create me a..."
            style={styles.textarea}
          ></textarea>
          <br />
        </form>
        <button style={(loading || input.length <= 0) ? { ...styles.button, ...styles.disabledButton } : { ...styles.button }} type="submit" onClick={(e) => handleDoc(e)}>Buat Dokumen</button>
        <button style={downloadable ? { ...styles.downloadButton } : { ...styles.downloadButton, ...styles.disabledButton }} onClick={(e) => handleDownload(e)}>Download</button>
        <br />
        <br />
        <FileUpload handleUpload={(file) => handleUpload(file)} disabled={loading || input.length <= 0} />
        <a href={whatsappUrl} target="_blank" rel="noopener noreferrer">
          <Button text="Laporkan" />
        </a>
        <br />
        {loading && <div style={styles.loadingContainer}>
          <ClipLoader size={50} color="#fff" />
          <p style={styles.loadingText}>{loadingText}</p>
        </div>}
      </div>
      <div style={styles.parentTextArea}>
        <h4>Content Review: </h4>
        <textarea
          value={requestForDownload}
          onChange={(e) => setRequestForDownload(e.target.value)}
          rows="10"
          cols="50"
          disabled
          style={styles.fullHeightTextarea}
        ></textarea>
      </div>
    </div>
  );
}

const styles = {
  container12: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  outerContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '100%',
  },
  container: {
    textAlign: 'center',
    fontFamily: '"Segoe UI", Tahoma, Geneva, Verdana, sans-serif',
    background: 'linear-gradient(to right, #e7e7e7, #eeeeee)',
    height: '95vh',
    borderRadius: '8px',
    color: '#fff',
    maxWidth: '600px',
    boxShadow: '0 4px 8px rgba(0,0,0,0.1)',
  },
  loadingContainer: {
    marginTop: "18px"
  },
  header: {
    fontSize: '2.5em',
    marginBottom: '20px',
    color: '#63768E',
    marginLeft: '12px'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  textarea: {
    width: '80%',
    padding: '10px',
    borderRadius: '8px',
    border: 'none',
    fontSize: '1em',
    boxShadow: '0 2px 4px rgba(0,0,0,0.1)',
  },
  parentTextArea: {
    flex: '1',
    display: 'flex',
    height: '95vh',
    flexDirection: 'column',
    marginLeft: '24px',
    marginRight: '48px',
  },
  fullHeightTextarea: {
    flex: '1',
    padding: '10px',
    borderRadius: '2px',
    border: 'none',
    fontSize: '1em',
    boxShadow: '0 2px 4px rgba(0,0,0,0.1)',
    width: '100%',
    resize: 'none',
  },
  button: {
    backgroundColor: '#2575fc',
    border: 'none',
    color: 'white',
    padding: '10px 20px',
    textAlign: 'center',
    textDecoration: 'none',
    display: 'inline-block',
    fontSize: '1em',
    margin: '10px 10px',
    marginRight: "54px",
    cursor: 'pointer',
    borderRadius: '8px',
    boxShadow: '0 4px 6px rgba(0,0,0,0.1)',
    transition: 'background-color 0.3s, transform 0.3s',
  },
  downloadButton: {
    backgroundColor: '#1a11cb',
    border: 'none',
    color: 'white',
    padding: '10px 20px',
    textAlign: 'center',
    textDecoration: 'none',
    display: 'inline-block',
    fontSize: '1em',
    margin: '10px 10px',
    cursor: 'pointer',
    borderRadius: '8px',
    boxShadow: '0 4px 6px rgba(0,0,0,0.1)',
    transition: 'background-color 0.3s, transform 0.3s'
  },
  fixButton: {
    backgroundColor: '#155599',
  },
  disabledButton: {
    backgroundColor: '#e7e7e7',
  },
  loadingText: {
    marginTop: '10px',
    fontSize: '1.2em',
    color: "#000000",
  },
};


export default App;
