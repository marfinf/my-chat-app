const BASE_URL = process.env.REACT_APP_STAGE === 'production'
    ? 'https://tangdoc.id'
    : 'http://localhost:3001';

export { BASE_URL }
