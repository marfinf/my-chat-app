import React from 'react';
import { useDropzone } from 'react-dropzone';
import './FileUpload.css';

const FileUpload = ({ disabled, handleUpload }) => {
  const onDrop = (acceptedFiles) => {
    const file = acceptedFiles[0];
    if (file) {
      handleUpload(file);
    }
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: {
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document': ['.docx'],
    },
    disabled: disabled
  });

  return (
    <button {...getRootProps({ className: `dropzone ${disabled ? 'disabled' : ''}` })}>
      <input {...getInputProps()} />
      <p>Bedasarkan Referensi<br />Dokumenmu</p>
    </button>
  );
};

export default FileUpload;
