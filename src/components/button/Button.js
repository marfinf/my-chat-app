import React from 'react';
import './Button.css';

const Button = ({ icon, text, onClick }) => {
    return (
        <button onClick={onClick} className='button'>
            <span>{text} 🚨</span>
        </button>
    );
};

export default Button